<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<?php include 'layouts/head.php'; ?>
<!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
        <!-- BEGIN HEADER -->
        <?php include 'layouts/nav.php'; ?>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <?php include 'layouts/sidebar.php'; ?>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title"> Inicio</h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="index.php">Apple</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <span>Usuario</span>
                            </li>
                        </ul>
                    </div>
                    <!-- END PAGE HEADER-->
                    <div class="profile">
                        <div class="tabbable-line tabbable-full-width">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_1" data-toggle="tab" aria-expanded="true"> Resumen de Compras </a>
                                </li>
                                <li class="">
                                    <a href="#tab_1_3" data-toggle="tab" aria-expanded="false"> Cuenta </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1_1">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-8 profile-info">
                                                    <h1 class="font-green sbold uppercase">John Doe</h1>
                                                </div>
                                                <!--end col-md-8-->
                                                <div class="col-md-4">
                                                    <div class="portlet sale-summary">
                                                        <div class="portlet-title">
                                                            <div class="caption font-red sbold"> Informacion de compras </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end col-md-4-->
                                            </div>
                                            <!--end row-->
                                            <div class="tabbable-line tabbable-custom-profile">
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab_1_11">
                                                        <div class="portlet-body">
                                                            <table class="table table-striped table-bordered table-advance table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th> Producto </th>
                                                                        <th class="hidden-xs"> Estado de compra </th>
                                                                        <th> Precio </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td> iPad Pro </td>
                                                                        <td class="hidden-xs"> Entregado </td>
                                                                        <td> $27,900 </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> iPhone </td>
                                                                        <td class="hidden-xs"> Pendiente </td>
                                                                        <td> $18,999 </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Watch </td>
                                                                        <td class="hidden-xs"> Pendiente </td>
                                                                        <td> $397,000 </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--tab_1_2-->
                                <div class="tab-pane" id="tab_1_3">
                                    <div class="row profile-account">

                                        <div class="col-md-9">
                                            <div class="tab-content">
                                                <div id="tab_1-1" class="tab-pane active">
                                                    <form role="form" action="#">
                                                        <div class="form-group">
                                                            <label class="control-label">Nombre de Usuario</label>
                                                            <input type="text" placeholder="John" class="form-control"> </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Nombre</label>
                                                            <input type="text" placeholder="John" class="form-control"> </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Correo</label>
                                                            <input type="text" placeholder="john@outlook.en" class="form-control"> </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Domicilio</label>
                                                            <input type="text" placeholder="San Francisco 4380" class="form-control"> </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Ciudad</label>
                                                            <input type="text" placeholder="Guadalajara" class="form-control"> </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Pais</label>
                                                            <input type="text" placeholder="Mexico" class="form-control"> </div>
                                                        <div class="margiv-top-10">
                                                            <a href="javascript:;" class="btn green"> Guardar cambios </a>
                                                            <a href="user.php" class="btn default"> Cancelar </a>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end col-md-9-->
                                    </div>
                                </div>
                                <!--end tab-pane-->

                                <!--end tab-pane-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php include 'layouts/footer.php'; ?>
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
        <script src="assets/global/plugins/respond.min.js"></script>
        <script src="assets/global/plugins/excanvas.min.js"></script>
        <![endif]-->
    <?php include 'layouts/scripts.php'; ?>
    </body>
</html>
