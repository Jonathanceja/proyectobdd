<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<?php include 'layouts/head.php'; ?>
<!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
        <!-- BEGIN HEADER -->
        <?php include 'layouts/nav.php'; ?>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <?php include 'layouts/sidebar.php'; ?>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title"> Inicio</h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="index.php">Apple</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <span>Inicio</span>
                            </li>
                        </ul>
                    </div>
                    <!-- END PAGE HEADER-->
                    <div id="wrapper" style="text-align: center">
                          <div id="yourdiv" style="display: inline-block;"><h1>Apple</h1></div>
                    </div>
                    <div id="wrapper" style="text-align: center">
                          <div id="yourdiv" style="display: inline-block;"><h3>Apple es líder mundial en innovación con el iPhone, iPad, Mac, Apple Watch, iOS, OS X, watchOS y más.</h3></div>
                    </div>
                    <div class="row">
                      <div id="wrapper" style="text-align: center">
                            <div id="yourdiv" style="display: inline-block;"><h3>"Think Different"</h3></div>
                      </div>
                    </div>
                    <div class="row">
                      <div id="wrapper" style="text-align: center">
                            <img src="assets\products\products_hero.png" alt="" />
                      </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php include 'layouts/footer.php'; ?>
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
        <script src="assets/global/plugins/respond.min.js"></script>
        <script src="assets/global/plugins/excanvas.min.js"></script>
        <![endif]-->
    <?php include 'layouts/scripts.php'; ?>
    </body>
</html>
