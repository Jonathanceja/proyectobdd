<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item start active open">
                <a href="index.php" class="nav-link nav-toggle">
                    <i class="fa fa-apple"></i>
                    <span class="title">Apple</span>
                </a>
            </li>
            <li class="nav-item start active open">
                <a href="iPhone.php" class="nav-link nav-toggle">
                    <i class="fa fa-mobile-phone"></i>
                    <span class="title">iPhone</span>
                </a>
            </li>
            <li class="nav-item start active open">
                <a href="iPod.php" class="nav-link nav-toggle">
                    <i class="fa fa-mobile-phone"></i>
                    <span class="title">iPod</span>
                </a>
            </li>
            <li class="nav-item start active open">
                <a href="iPad.php" class="nav-link nav-toggle">
                    <i class="fa fa-mobile-phone"></i>
                    <span class="title">iPad</span>
                </a>
            </li>
            <li class="nav-item start active open">
                <a href="Watch.php" class="nav-link nav-toggle">
                    <i class="icon-clock"></i>
                    <span class="title">Watch</span>
                </a>
            </li>
            <li class="nav-item start active open">
                <a href="Macbook.php" class="nav-link nav-toggle">
                    <i class="fa fa-laptop"></i>
                    <span class="title">Macbook</span>
                </a>
            </li>
            <li class="nav-item start active open">
                <a href="iMac.php" class="nav-link nav-toggle">
                    <i class="fa fa-desktop"></i>
                    <span class="title">iMac</span>
                </a>
            </li>
            <li class="nav-item start active open">
                <a href="TV.php" class="nav-link nav-toggle">
                    <i class="fa fa-tv"></i>
                    <span class="title">TV</span>
                </a>
            </li>
            <li class="nav-item start active open">
                <a href="soporte.php" class="nav-link nav-toggle">
                    <i class="fa fa-gear"></i>
                    <span class="title">Soporte</span>
                </a>
            </li>
        </ul>
    </div>
</div>
