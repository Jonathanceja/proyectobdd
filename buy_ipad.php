<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<?php include 'layouts/head.php'; ?>
<!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
        <!-- BEGIN HEADER -->
        <?php include 'layouts/nav.php'; ?>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <?php include 'layouts/sidebar.php'; ?>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title"> Comprar iPad </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="index.php">Apple</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <span>Comprar iPad</span>
                            </li>
                        </ul>
                    </div>
                    <!-- END PAGE HEADER-->
                    <div class="portlet box purple ">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                         Comprar iPad </div>
                                                    </div>
                                                    <div class="portlet-body form">
                                                        <form class="form-horizontal" role="form">
                                                            <div class="form-body">
                                                                  <h3>iPad</h3>
                                                                  <div class="row">
                                                                    <div class="col-md-3">
                                                                      <img src="assets\products\ipad_sma.png" alt="" />
                                                                    </div>
                                                                    <div class="form-group">
                                                                    <label class="col-md-3 control-label">Precio:</label>
                                                                    <div class="col-md-3">
                                                                      <h3>64GB $15,800</h3>
                                                                      <h3>128GB $21,600</h3>
                                                                      <h3>256GB $27,900</h3>
                                                                      <div class="form-group">
                                                                          <label class="">Color:</label>
                                                                          <div class="row">
                                                                              <select class="form-control">
                                                                                  <option>Gris Espacial</option>
                                                                                  <option>Oro</option>
                                                                                  <option>Oro Rosa</option>
                                                                                  <option>Plateado</option>
                                                                              </select>
                                                                          </div>

                                                                          <label class="">Capacidad:</label>
                                                                          <div class="row">
                                                                              <select class="form-control">
                                                                                  <option>64 GB</option>
                                                                                  <option>128 GB</option>
                                                                                  <option>256 GB</option>
                                                                              </select>
                                                                          </div>
                                                                      </div>
                                                                    </div>
                                                                </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-actions right1">
                                                                <button type="submit" class="btn green">Comprar</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php include 'layouts/footer.php'; ?>
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
        <script src="assets/global/plugins/respond.min.js"></script>
        <script src="assets/global/plugins/excanvas.min.js"></script>
        <![endif]-->
    <?php include 'layouts/scripts.php'; ?>
    </body>
</html>
